#include "Item.h"
#include "DxLib.h"
#include "main.h"

int Item::Initialize(void) {
	setitem = 0;
	if ((sound_eat_fruit= LoadSoundMem("Resource/Sounds/SE/Eat_fruit.wav")) == -1)return -1;
	return 0;
}

void Item::SetItem(Map &map) {
	
	if (++timer < 600) { map.SetMap(Item_X, Item_Y, 'c'); setitem = Item_Cherry;}
	else { map.SetMap(Item_X, Item_Y, 'B'); setitem = 0;}
}

int Item::GetItem(Map &map, Player &player) {

	DrawItem();

	if (GetItemList(map, player) == 1) {
		PlaySoundMem(sound_eat_fruit, DX_PLAYTYPE_BACK, TRUE);
		switch (map.GetMap(player.GetPlayerX() + player.GetAnalogSelectInputX(), player.GetPlayerY() + player.GetAnalogSelectInputY()))
		{
		case 'c':
			player.AddScore(100,true);
			break;
		case 'a':
			player.AddScore(700, true);
			break;

		case 's':
			player.AddScore(300, true);
			break;

		case '@':
			player.AddScore(500, true);
			break;

		case 'M':
			player.AddScore(1000, true);
			break;

		case 'S':
			player.AddScore(3000, true);
			break;

		case 'k':
			player.AddScore(5000, true);
			break;

		case 'g':
			player.AddScore(2000, true);
			break;


		default:
			break;
		}
		map.SetMap(player.GetPlayerX() + player.GetAnalogSelectInputX(), player.GetPlayerY() + player.GetAnalogSelectInputY(), 'B');
		setitem = 0;
	}

	if (map.GetMap(player.GetPlayerX() + player.GetAnalogSelectInputX(), player.GetPlayerY()) == 'c') {//さくらんぼ
		
	}
	return 0;
}

int Item::GetItemList(Map &map, Player &player) {
	switch (map.GetMap(player.GetPlayerX() + player.GetAnalogSelectInputX(), player.GetPlayerY() + player.GetAnalogSelectInputY()))
	{
	case 'c':	//さくらんぼ
		return 1;
		break;

	case 'a':	//りんご
		return 1;
		break;

	case 's':	//いちご
		return 1;
		break;

	case '@':	//みかん
		return 1;
		break;

	case 'M':	//メロン
		return 1;
		break;

	case 'S':	//ベル
		return 1;
		break;

	case 'k':	//キー
		return 1;
		break;

	case 'g':	//ボス・ギャラクシアン
		return 1;
		break;

	default:
		return 0;
		break;
	}
}

void Item::DrawItem() {
	//			アイテム
	if (setitem == Item_Cherry) { DrawGraph(Item_X * MAP_SIZE + MAP_PositionX, Item_Y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Items, 0), TRUE); }		//さくらんぼ
	if (setitem == Item_Apple) { DrawGraph(Item_X * MAP_SIZE + MAP_PositionX, Item_Y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Items, 1), TRUE); }		//りんご
	if (setitem == Item_Strawberry) { DrawGraph(Item_X * MAP_SIZE + MAP_PositionX, Item_Y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Items, 2), TRUE); }		//いちご
	if (setitem == Item_Orange) { DrawGraph(Item_X * MAP_SIZE + MAP_PositionX, Item_Y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Items, 3), TRUE); }		//みかん
	if (setitem == Item_Melon) { DrawGraph(Item_X * MAP_SIZE + MAP_PositionX, Item_Y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Items, 4), TRUE); }		//メロン
	if (setitem == Item_Bell) { DrawGraph(Item_X * MAP_SIZE + MAP_PositionX, Item_Y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Items, 5), TRUE); }		//ベル
	if (setitem == Item_Key) { DrawGraph(Item_X * MAP_SIZE + MAP_PositionX, Item_Y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Items, 6), TRUE); }		//キー
	if (setitem == Item_Boss) { DrawGraph(Item_X * MAP_SIZE + MAP_PositionX, Item_Y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Items, 7), TRUE); }		//ボス・ギャラクシアン
}