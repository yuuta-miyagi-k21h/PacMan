#pragma once
#include "Map.h"


class Title {
	int img_namco_logo;
	int img_copyright;
	int TitleFont;
	int Credit;
	bool StartFlg;
	int timer;
	int AnimationTimer;
	float AnimationX;
	int Animation_EnemyX;
	int PauseTime;
	bool AnimationEndFlg;
public:
	int Initialize(void);
	int DrawTitle(int Keyflg,Map &map);
};