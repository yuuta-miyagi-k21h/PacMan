#include "enemy.h"
#include "DxLib.h"
#include "main.h"
Enemy enemy[4];

int Enemy::Initialize(void) {
	if ((sound_eat_ghost = LoadSoundMem("Resource/Sounds/SE/Eat_ghost.wav")) == -1)return -1;
	if ((sound_ghost_normalmove = LoadSoundMem("Resource/Sounds/SE/Ghost_NormalMove.wav")) == -1)return -1;
	if ((sound_ghost_return = LoadSoundMem("Resource/Sounds/SE/Return to Ghost.wav")) == -1)return -1;
	enemy[0].x = 12;
	enemy[0].y = 14;

	for(int i=0;i<4;i++){
		enemy[i].type = i;
		enemy[i].flg = false;
		enemy[i].directionX = 1;
		enemy[i].directionY = 0;
		enemy[i].ghostflg = false;
		enemy[i].deathflg = false;
	}
	return 0;
}

void Enemy::CreateEnemy(int type,Map &map, Player &player) {
	for (int i = 0; i < 4; i++) {
		if (enemy[i].type == type) { 
			enemy[i].flg = true; 
			enemy[i].ghostflg = player.Isflirting_monster();

			//イジケモンスター
			if ((player.GetPlayerX() == enemy[i].x) && (player.GetPlayerY() == enemy[i].y)) {
				enemy[i].deathflg = true;
				if(!CheckSoundMem(sound_ghost_return))PlaySoundMem(sound_ghost_return, DX_PLAYTYPE_BACK, TRUE);
				enemy[i].x = Enemy_SpawnX;
				enemy[i].y = Enemy_SpawnY;
				player.SetLife(player.GetLife() - 1);
				PlaySoundMem(sound_eat_ghost, DX_PLAYTYPE_BACK, TRUE);
			}
		}
	}

	if(type == SHADOW_BLINKY)SHADOW_BLINKY_Move(map,player);
}

void Enemy::SHADOW_BLINKY_Move(Map map, Player player) {
	static int MoveInterval;
	static int ReturnTime = 120;	//戻る時間
	static int GhostTime = GHOST_TIME;
	static int distance;
	if (enemy[0].flg == true) {
		++MoveInterval;
		/*if (enemy[0].direction == 1 && (map.GetMap(enemy[0].x + 1, enemy[0].y) == 'B' || map.GetMap(enemy[0].x + 1, enemy[0].y) == '0'))if (MoveInterval > 10) { ++enemy[0].x; MoveInterval = 0; }
		if (enemy[0].direction == -1 && (map.GetMap(enemy[0].x - 1, enemy[0].y) == 'B' || map.GetMap(enemy[0].x - 1, enemy[0].y) == '0'))if (MoveInterval > 10) { --enemy[0].x; MoveInterval = 0; }
		if (enemy[0].direction == 2 && (map.GetMap(enemy[0].x, enemy[0].y - 1) == 'B' || map.GetMap(enemy[0].x, enemy[0].y + 1) == '0'))if (MoveInterval > 10) { ++enemy[0].y; MoveInterval = 0; }
		if (enemy[0].direction == -2 && (map.GetMap(enemy[0].x, enemy[0].y - 1) == 'B' || map.GetMap(enemy[0].x, enemy[0].y - 1) == '0'))if (MoveInterval > 10) { --enemy[0].y; MoveInterval = 0; }*/
		DrawFormatString(900, 500, 0x00FFFFFF, "%d", enemy[0].directionX);
		DrawFormatString(900, 550, 0x00FFFFFF, "%d", enemy[0].directionY);

		int bufx = enemy[0].x;
		int bufy = enemy[0].y;
		int Min_distance = 1000;
		static int Vectol;

		/*for (int dirX = 0; dirX < 1; dirX++) {
			if ((map.GetMap(bufx, bufy) != 'B') || (map.GetMap(bufx, bufy) != '0') || (map.GetMap(bufx, bufy) != 'D'))continue;
			distance = (enemy[0].x - player.GetPlayerX() + dirX) * (enemy[0].x - player.GetPlayerX() + dirX);
		}*/
		//distance = (enemy[0].y - player.GetPlayerY()) * (enemy[0].y - player.GetPlayerY()) + (enemy[0].x - player.GetPlayerX()) * (enemy[0].x - player.GetPlayerX());
		DrawFormatString(100, 100, 0xFFFFFF, "%d", Min_distance);
		DrawFormatString(100, 200, 0xFFFFFF, "Vec:%d", Vectol);
		
		static int distance_dis;
		DrawFormatString(100, 600, 0xFFFFFF, "Vec:%d", distance_dis);
		for (int i = 0; i < 2; i++) {
			//if ((map.GetMap(bufx + (i == 1 ? 1 : -1), bufy + (i ? 1 : -1)) != 'B'))continue;
			distance = (enemy[0].y - player.GetPlayerY()) * (enemy[0].y - player.GetPlayerY()) + (enemy[0].x - player.GetPlayerX() + (i == 1 ? 1:-1)) * (enemy[0].x - player.GetPlayerX() + (i == 1? 1 : -1));
			if (distance < Min_distance) { Min_distance = distance; Vectol = i; i = 0; distance_dis = Min_distance; }
		}
		for (int j = 0; j < 2; j++) {
			//if ((map.GetMap(bufx + (j == 1? 1 : -1), bufy + (j ? 1 : -1)) != 'B'))continue;
			distance = (enemy[0].y - player.GetPlayerY() + (j == 1 ? 1 : -1)) * (enemy[0].y - player.GetPlayerY() + (j == 1 ? 1 : -1)) + (enemy[0].x - player.GetPlayerX()) * (enemy[0].x - player.GetPlayerX());
			if (distance < Min_distance) { Min_distance = distance; Vectol = j ? 3 : 2; j = 0; }
		}




		if (MoveInterval > 15) {
			if(!CheckSoundMem(sound_ghost_normalmove))PlaySoundMem(sound_ghost_normalmove, DX_PLAYTYPE_BACK, TRUE);
			if (/*player.GetPlayerX() > bufx*/ Vectol == 1) { ++bufx; enemy[0].directionX = 1; }
			else if (/*player.GetPlayerX() < bufx*/Vectol == 0) { --bufx; enemy[0].directionX = -1; }
			if (/*player.GetPlayerY() > bufy*/ Vectol == 3) { ++bufy; enemy[0].directionY = 1; }
			else if (/*player.GetPlayerY() < bufy*/ Vectol == 2) { --bufy; enemy[0].directionY = -1; }
			MoveInterval = 0;
		}
		if ((map.GetMap(bufx, bufy) == 'B') || (map.GetMap(bufx, bufy) == '0') || (map.GetMap(bufx, bufy) == 'D')) { enemy[0].x = bufx; enemy[0].y = bufy; }


		//if ((map.GetMap(enemy[0].x + enemy[0].direction, enemy[0].y) != 'B')) {
		//	if (enemy[0].direction == 1) { enemy[0].direction = -1; }
		//	else if (enemy[0].direction == -1) { enemy[0].direction = 1; }
		//}
		//if ((map.GetMap(enemy[0].x, enemy[0].y + enemy[0].direction % 2) != 'B' || map.GetMap(enemy[0].x + enemy[0].direction % 2, enemy[0].y) != '0')) {
		//if (enemy[0].direction == 2)enemy[0].direction = -2;
		//if (enemy[0].direction == -2)enemy[0].direction = 2;
		//}

		//アカベイ
		if (ghostflg == false && deathflg == false) {
			if (++AniTimer < 10) {
				if (enemy[0].directionX == -1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 0), TRUE);
				if (enemy[0].directionX == 1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 1), TRUE);
				if (enemy[0].directionY == -1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 2), TRUE);
				if (enemy[0].directionY == 1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 3), TRUE);
			}
			if (AniTimer >= 10) {
				if (enemy[0].directionX == -1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 24), TRUE);
				if (enemy[0].directionX == 1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 25), TRUE);
				if (enemy[0].directionY == -1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 26), TRUE);
				if (enemy[0].directionY == 1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 27), TRUE);
			}
			if (AniTimer >= 20) { AniTimer = 0; }
		}
	}

	if (enemy[0].deathflg == true) {
		if (--ReturnTime > 0) {
			if (enemy[0].directionX == -1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 16), TRUE);
			if (enemy[0].directionX == 1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 17), TRUE);
			if (enemy[0].directionY == -1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 18), TRUE);
			if (enemy[0].directionY == 1)DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 19), TRUE);
		}
		else { enemy[0].deathflg = false; ReturnTime = 120; }
	}

	if (enemy[0].ghostflg == true) {
		if (--GhostTime > 0) {
			if (++AniTimer < 10) {
				DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 20), TRUE);
			}
			if (AniTimer >= 10) {
				DrawGraph(enemy[0].x * MAP_SIZE + MAP_PositionX - 10, enemy[0].y * MAP_SIZE + MAP_PositionY - 10, GetArrayImages(AImage_Enemys, 21), TRUE);
			}
			if (AniTimer >= 20) { AniTimer = 0; }
		}
		else {enemy[0].ghostflg = false; player.Setflirting_monster(false); /*GhostTime = GHOST_TIME;*/ }
	}
}