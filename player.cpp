#include "player.h"
#include "main.h"
#include "DxLib.h"

int Player::Initialize(void) {
	SelectX = 0;
	SelectY = 0;
	score = 0;
	score_display = 0;
	speed = 1;
	deathflg = false;
	life = PLAYER_LIFE;
	x = PLAYER_INITX; y = PLAYER_INITY;
	if ((sound_Power_cookie = LoadSoundMem("Resource/Sounds/SE/Powercookie.wav")) == -1)return -1;
	ScoreFont = CreateFontToHandle("Emulogic", 10, 6, DX_FONTTYPE_NORMAL);
	return 0;
}

void Player::DrawPlayer(Map& map, int type) {
	
	++PlayerControlTimer;
	if (life == 0)DeathPlayer();

	if (map.GetMap(x + SelectX,y + SelectY) == 'B' && PlayerControlTimer > 10) {
		if (SelectX == 1) {  ++x;}
		else if (SelectX == -1) { --x;}
		else if (SelectY == 1) { ++y;}
		else if (SelectY == -1) {  --y;}
		PlayerControlTimer = 0;
	}
		//		ループトンネル
		if(SelectX == -1 && (x == 0 && y == 14)){x = 27;}
		else if (SelectX == 1 && (x == 27 && y == 14)) { x = 0; }


		//エサ食べる
		static int eat_count;
		if (map.GetMap(x + SelectX, y + SelectY) == '0') { 
			score += 10;
			map.SetMap(x + SelectX, y + SelectY, 'B');
			PlaySoundMem(GetSounds(Sound_Eat_Cookies), DX_PLAYTYPE_BACK, TRUE);
			++eat_count;
		}
		DrawExtendFormatString(100, 300, 0.5f, 0.5f, 0xFFFFFF, "%d", eat_count);
		DrawExtendFormatString(100, 320, 0.5f, 0.5f, 0xFFFFFF, "%d", SelectX);
		DrawExtendFormatString(100, 350, 0.5f, 0.5f, 0xFFFFFF, "%d", SelectY);
		if ((eat_count == 90) || (eat_count == 140));//アイテム取得用

		//パワーエサ
		if (map.GetMap(x + SelectX, y + SelectY) == 'o') { 
			map.SetMap(x + SelectX, y + SelectY, 'B');
			score += 50;
			flirting_monster = true;
			if(CheckSoundMem(!sound_Power_cookie))PlaySoundMem(sound_Power_cookie, DX_PLAYTYPE_BACK, TRUE);
		}

		
		
	if (type == PLAYER_NORMAL && deathflg == false) {
		if(SelectX == 0 && SelectY == 0)DrawGraph(x * MAP_SIZE + MAP_PositionX, y * MAP_SIZE + MAP_PositionY - 9, GetArrayImages(AImage_Pacman, 0), TRUE);
		if (++AniTimer < 10 && PlayerControlTimer < 10) {
			if(SelectX == -1)DrawGraph(x * MAP_SIZE + MAP_PositionX,y * MAP_SIZE + MAP_PositionY - 9, GetArrayImages(AImage_Pacman, 1), TRUE);
			if (SelectX == 1)DrawTurnGraph(x * MAP_SIZE + MAP_PositionX - 10, y * MAP_SIZE + MAP_PositionY - 9, GetArrayImages(AImage_Pacman, 1), TRUE);
			if (SelectY == -1)DrawRotaGraph(x * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, y * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0F, PI / 2, GetArrayImages(AImage_Pacman, 1), TRUE);
			if (SelectY == 1)DrawRotaGraph(x * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, y * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0F, PI / 2, GetArrayImages(AImage_Pacman, 1), TRUE,TRUE);
		}
		if (AniTimer >= 10 || PlayerControlTimer > 11) {
			if (SelectX == -1)DrawGraph(x * MAP_SIZE + MAP_PositionX, y * MAP_SIZE + MAP_PositionY - 9, GetArrayImages(AImage_Pacman, 2), TRUE);
			if (SelectX == 1)DrawTurnGraph(x * MAP_SIZE + MAP_PositionX - 10, y * MAP_SIZE + MAP_PositionY - 9, GetArrayImages(AImage_Pacman, 2), TRUE);
			if (SelectY == -1)DrawRotaGraph(x * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, y * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0F, PI / 2, GetArrayImages(AImage_Pacman, 2), TRUE);
			if (SelectY == 1)DrawRotaGraph(x * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, y * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0F, PI / 2, GetArrayImages(AImage_Pacman, 2), TRUE, TRUE);
		}
		if(AniTimer >= 20 && PlayerControlTimer < 10){ DrawGraph(x * MAP_SIZE + MAP_PositionX, y * MAP_SIZE + MAP_PositionY - 9, GetArrayImages(AImage_Pacman, 0), TRUE); AniTimer = 0; }
	}
	static int score_displaytime;
	if (score_display > 0) {
		if (++score_displaytime < 80)DrawFormatStringToHandle(x * MAP_SIZE + MAP_PositionX,   y * MAP_SIZE + MAP_PositionY - 5, 0xFF00FF,ScoreFont, "%d", score_display);
	}
	else if (AniTimer >= 80) { score_display = 0; score_displaytime = 120; }
}

void Player::DeathPlayer(void) {
	static int Anitimer;
	static int AniCount;
	deathflg = true;
	if (++Anitimer < 30)DrawRotaGraph(x * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, y * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0F, PI / 2, GetArrayImages(AImage_BreakPacman, AniCount++), TRUE);
	if (AniTimer >= 330) { AniTimer = 0; deathflg = false; }
}

int Player::UpdateAnalogInput(bool flg) {
	GetJoypadAnalogInput(&JoyPadX, &JoyPadY, DX_INPUT_PAD1);
	InputKey = GetJoypadInputState(DX_INPUT_PAD1);
	SetJoypadDeadZone(DX_INPUT_PAD1, 0.3f);     //角度の制限を緩和

	if (flg == true) {
		if (++PadTimer > 10) {
			PadTimer = 0;
			if (JoyPadX > 100 || InputKey & 4) { SelectX = 1; SelectY = 0; }
			else if (JoyPadX < -100 || InputKey & 2) { SelectX = -1; SelectY = 0; }
			else if (JoyPadY > 100 || InputKey & 1){SelectY = 1; SelectX = 0;}
			else if (JoyPadY < -100 || InputKey & 8) { SelectY = -1; SelectX = 0; }
		}
	}
	return 0;
}

int Player::GetAnalogSelectInputX(void) {
	if (SelectX == 1 || SelectX == -1) { return SelectX; }
	else { return 0; }
}

int Player::GetAnalogSelectInputY(void) {
	if (SelectY == 1 || SelectY == -1) { return SelectY; }
	else { return 0; }
}

int Player::GetPlayerX(void) {
	if (x >= 0)return x;
	return -1;
}

int Player::GetPlayerY(void) {
	if (y >= 0)return y;
	return -1;
}

int Player::GetLife(void) {
	return life;
}

void Player::SetLife(int num) {
	if (num >= 0 && num < 8)life = num;
}

int Player::GetScore(void) {
	return score;
}

void Player::AddScore(int num, bool display) {
	if (num >= 0)score += num;
	if (display == true)score_display = num;
}

bool Player::Isflirting_monster(void) {
	return flirting_monster;
}

void Player::Setflirting_monster(bool is) {
	flirting_monster = is;
}