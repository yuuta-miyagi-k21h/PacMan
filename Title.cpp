#include "Title.h"
#include "Map.h"
#include "DxLib.h"
#include "main.h"

int Title::Initialize(void) {
	if ((img_namco_logo = LoadGraph("Resource/Images/namco_logo.png")) == -1)return -1;
	if ((img_copyright = LoadGraph("Resource/Images/Copyright.png")) == -1)return -1;
	TitleFont = CreateFontToHandle("Emulogic", 20, 6, DX_FONTTYPE_NORMAL);
	StartFlg = false;
	Credit = 0;
	timer = 0;
	AnimationX = 0;
	Animation_EnemyX = 0;
	PauseTime = 0;
	AnimationEndFlg = false;
	return 0;
}

int Title::DrawTitle(int Keyflg, Map& map) {
	++timer;
	if (StartFlg == false) {

		if (AnimationEndFlg == false) {
			DrawStringToHandle(MAP_PositionX + 50, MAP_PositionY + 120, "CHARACTER / NICKNAME", 0xFFFFFF, TitleFont);
			if (timer > 20)DrawGraph(MAP_PositionX + 10, MAP_PositionY + 160, GetArrayImages(AImage_Enemys, 1), TRUE);
			if (timer > 60)DrawStringToHandle(MAP_PositionX + 72, MAP_PositionY + 166, "OIKAKE----", 0xEE0709, TitleFont);
			if (timer > 80)DrawStringToHandle(MAP_PositionX + 292, MAP_PositionY + 166, "\"AKABEI\"", 0xEE0709, TitleFont);
			if (timer > 100)DrawGraph(MAP_PositionX + 10, MAP_PositionY + 210, GetArrayImages(AImage_Enemys, 5), TRUE);
			if (timer > 140)DrawStringToHandle(MAP_PositionX + 72, MAP_PositionY + 216, "MACHIBUSE--", 0xF0BCEB, TitleFont);
			if (timer > 160)DrawStringToHandle(MAP_PositionX + 312, MAP_PositionY + 216, "\"PINKY\"", 0xF0BCEB, TitleFont);
			if (timer > 180)DrawGraph(MAP_PositionX + 10, MAP_PositionY + 260, GetArrayImages(AImage_Enemys, 9), TRUE);
			if (timer > 220)DrawStringToHandle(MAP_PositionX + 72, MAP_PositionY + 266, "KIMAGURE--", 0x26F6F0, TitleFont);
			if (timer > 240)DrawStringToHandle(MAP_PositionX + 292, MAP_PositionY + 266, "\"AOSUKE\"", 0x26F6F0, TitleFont);
			if (timer > 280)DrawGraph(MAP_PositionX + 10, MAP_PositionY + 310, GetArrayImages(AImage_Enemys, 13), TRUE);
			if (timer > 320)DrawStringToHandle(MAP_PositionX + 72, MAP_PositionY + 316, "OTOBOKE---", 0xEEB963, TitleFont);
			if (timer > 340)DrawStringToHandle(MAP_PositionX + 292, MAP_PositionY + 316, "\"GUZUTA\"", 0xEEB963, TitleFont);

			if (timer > 420) {
				DrawCircleAA(MAP_PositionX + 130, MAP_PositionY + 432, 1.8f, 10, 0xF0CCCA, TRUE);
				DrawStringToHandle(MAP_PositionX + 160, MAP_PositionY + 420, "10", 0xFFFFFF, TitleFont);
				DrawExtendStringToHandle(MAP_PositionX + 220, MAP_PositionY + 425, 0.8f, 0.8f, "PTS", 0xFFFFFF, TitleFont);
				if (timer <= 580)DrawCircleAA(MAP_PositionX + 130, MAP_PositionY + 464, 10.0f, 10, 0xF0CCCA, TRUE);
				DrawStringToHandle(MAP_PositionX + 160, MAP_PositionY + 450, "50", 0xFFFFFF, TitleFont);
				DrawExtendStringToHandle(MAP_PositionX + 220, MAP_PositionY + 455, 0.8f, 0.8f, "PTS", 0xFFFFFF, TitleFont);
			}

			if (timer > 500) {
				if (timer <= 580)DrawCircleAA(MAP_PositionX + 15, MAP_PositionY + 380, 10.0f, 10, 0xF0CCCA, TRUE);
				DrawRotaGraph(MAP_PositionX + 200, MAP_PositionY + 530, 0.78f, 0, img_namco_logo, TRUE);
			}

			if (timer > 580) {
				if (!PauseTime) {
					if (timer <= 760)AnimationX -= 2.5f;
					else if (timer > 760)AnimationX += 2.5f;
					else if (timer > 860)AnimationX += 3.5f;
					if (timer % 10 == 0 && timer <= 760)Animation_EnemyX--;
					if (timer % 5 == 0 && timer > 760)Animation_EnemyX -= 5;
				}

				{//アニメーション表示
					if (PauseTime == 0) {
						++AnimationTimer;
						if(AnimationTimer >= 15)AnimationTimer = 0;
						if (AnimationTimer < 7) {
							if (timer <= 760)DrawCircleAA(MAP_PositionX + 15, MAP_PositionY + 385, 10.0f, 10, 0xF0CCCA, TRUE);
							DrawCircleAA(MAP_PositionX + 130, MAP_PositionY + 464, 10.0f, 10, 0xF0CCCA, TRUE);
						}

						if (AnimationTimer < 10) {
							if (timer <= 760) {
								DrawGraphF(500 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 0), TRUE);
								DrawGraphF(540 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 4), TRUE);
								DrawGraphF(580 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 8), TRUE);
								DrawGraphF(620 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 12), TRUE);
							}
							else {
								if (timer <= 800)DrawGraphF(500 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 20), TRUE);
								if (timer <= 890)DrawGraphF(540 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 20), TRUE);
								if (timer <= 980)DrawGraphF(580 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 20), TRUE);
								if (timer <= 1070)DrawGraphF(620 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 20), TRUE);
							}
						}
						if (AnimationTimer >= 10) {
							if (timer <= 760) {
								DrawGraphF(500 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 24), TRUE);
								DrawGraphF(540 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 28), TRUE);
								DrawGraphF(580 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 32), TRUE);
								DrawGraphF(620 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 36), TRUE);
							}
							else {
								if (timer <= 800)DrawGraphF(500 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 21), TRUE);
								if (timer <= 890)DrawGraphF(540 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 21), TRUE);
								if (timer <= 980)DrawGraphF(580 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 21), TRUE);
								if (timer <= 1070)DrawGraphF(620 + MAP_PositionX + AnimationX + Animation_EnemyX, 370 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 21), TRUE);
							}
						}


						if (AnimationTimer < 5) {
							if (timer <= 760)DrawGraphF(450 + MAP_PositionX - 10 + AnimationX, 370 + MAP_PositionY - 4, GetArrayImages(AImage_Pacman, 1), TRUE);
							else DrawTurnGraphF(450 + MAP_PositionX + AnimationX, 370 + MAP_PositionY - 4, GetArrayImages(AImage_Pacman, 1), TRUE);
						}
						if (AnimationTimer >= 5 && AnimationTimer < 10) {
							if (timer <= 760)DrawGraphF(450 + MAP_PositionX + AnimationX, 370 + MAP_PositionY - 4, GetArrayImages(AImage_Pacman, 2), TRUE);
							else DrawTurnGraphF(450 + MAP_PositionX + AnimationX, 370 + MAP_PositionY - 4, GetArrayImages(AImage_Pacman, 1), TRUE);
						}

						if (AnimationTimer >= 10) {
							DrawGraphF(450 + MAP_PositionX + AnimationX - 10, 370 + MAP_PositionY - 4, GetArrayImages(AImage_Pacman, 0), TRUE);
						}
					}
				}
				DrawFillBox(520 + MAP_PositionX, 340 + MAP_PositionY, 800 + MAP_PositionX, 420 + MAP_PositionY, 0x000000);	//演出用黒
			}

			if (timer >= 800 && timer <= 860) { PauseTime = 1; DrawRotaGraphF(520 + MAP_PositionX + AnimationX + Animation_EnemyX, 380 + MAP_PositionY + 5, 1.2f, 0, GetArrayImages(AImage_Scores,0), TRUE);}
			else if (timer >= 890 && timer <= 950) { PauseTime = 1; DrawRotaGraphF(560 + MAP_PositionX + AnimationX + Animation_EnemyX, 380 + MAP_PositionY + 5, 1.2f,0, GetArrayImages(AImage_Scores, 1), TRUE);}
			else if (timer >= 980 && timer <= 1040) { PauseTime = 1; DrawRotaGraphF(600 + MAP_PositionX + AnimationX + Animation_EnemyX, 380 + MAP_PositionY + 5, 1.2f, 0, GetArrayImages(AImage_Scores, 2), TRUE);
			}
			else if (timer >= 1070 && timer <= 1130) { PauseTime = 1; DrawRotaGraphF(640 + MAP_PositionX + AnimationX + Animation_EnemyX, 380 + MAP_PositionY + 5, 1.2f, 0, GetArrayImages(AImage_Scores, 3), TRUE);
			}
			else PauseTime = 0;

			if (timer > 1150)AnimationEndFlg = true;
		}
		if (AnimationEndFlg == true && timer < 1190) {
			map.DrawMap();
			DrawGraph(13 * MAP_SIZE + MAP_PositionX, 23 * MAP_SIZE + MAP_PositionY - 9, GetArrayImages(AImage_Pacman, 0), TRUE);
			DrawGraph(260 + MAP_PositionX, 210 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 0), TRUE);
			DrawGraph(260 + MAP_PositionX, 280 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 7), TRUE);
			DrawGraph(220 + MAP_PositionX, 280 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 10), TRUE);
			DrawGraph(300 + MAP_PositionX, 280 + MAP_PositionY - 5, GetArrayImages(AImage_Enemys, 14), TRUE);
			DrawExtendStringToHandle(180 + MAP_PositionX, 336 + MAP_PositionY, 0.9f, 0.9f, "GAME  OVER", 0xD10008, TitleFont);
			

		}
		if (AnimationEndFlg == true && timer > 1190) {
			DrawStringToHandle(MAP_PositionX + 70, MAP_PositionY + 250, "PUSH START BUTTON", 0xE5B983, TitleFont);
			DrawStringToHandle(MAP_PositionX + 115, MAP_PositionY + 350, "1 PLAYER ONLY", 0x26EFE1, TitleFont);
			DrawStringToHandle(MAP_PositionX + 0, MAP_PositionY + 430, "BONUS PUCKMAN FOR 10000", 0xE9A9AC, TitleFont);
			DrawExtendStringToHandle(MAP_PositionX + 530, MAP_PositionY + 435, 0.8f, 0.8f, "PTS", 0xE9A9AC, TitleFont);
			DrawRotaGraph(MAP_PositionX + 100, MAP_PositionY + 532, 0.82f, 0, img_copyright, TRUE);
			DrawRotaGraph(MAP_PositionX + 210, MAP_PositionY + 530, 0.78f, 0, img_namco_logo, TRUE);
			DrawStringToHandle(MAP_PositionX + 320, MAP_PositionY + 520, "1980", 0xD3A5CF, TitleFont);
			Credit = 1;
		}

		if (timer >= 1800)if (Initialize() == -1)DxLib_End();

		{//常時前面表示
			DrawStringToHandle(MAP_PositionX + 0, MAP_PositionY + 20, "1UP", 0xFFFFFF, TitleFont);
			DrawStringToHandle(MAP_PositionX + 42, MAP_PositionY + 45, "00", 0xFFFFFF, TitleFont);
			DrawStringToHandle(MAP_PositionX + 450, MAP_PositionY + 20, "2UP", 0xFFFFFF, TitleFont);
			DrawStringToHandle(MAP_PositionX + 150, MAP_PositionY + 20, "HIGH SCORE", 0xFFFFFF, TitleFont);
			DrawFormatStringToHandle(MAP_PositionX + 192, MAP_PositionY + 45, 0xFFFFFF, TitleFont, "%6d", GetHighScore());
			DrawFormatStringToHandle(MAP_PositionX, MAP_PositionY + 600, 0xFFFFFF, TitleFont, "CREDIT  %1d", Credit);
		}
	}

	if (Keyflg & 2048) { PlaySoundMem(GetSounds(Sound_Eat_Cookies), DX_PLAYTYPE_NORMAL, TRUE); StartFlg = true; }
	if (StartFlg == true)return 1;
	return 0;
}