#include "HUD.h"
#include "main.h"
#include "DxLib.h"

void HUD::Draw_HUD(Player& player) {

	DrawString(900, 100, "HIGH SCORE", 0xFFFFFF);
	DrawFormatString(950, 150, 0xFFFFFF, "%6d", GetHighScore());

	if (++Flashingtime < 15)DrawString(1000, 250, "1UP", 0xFFFFFF);
	else if (Flashingtime > 30)Flashingtime = 0;
	DrawFormatString(950, 300, 0xFFFFFF, "%6d", player.GetScore());

	//ライフ表示
	for (int i = 0; i < player.GetLife(); i++) {
		DrawGraph(50 * i + 900, 420, GetArrayImages(AImage_Pacman, 1), TRUE);
	}

	for (int i = 0; i < stage_displaynum; i++) {
		DrawGraph(-50 * i + 1200, 490, GetArrayImages(AImage_Items, 0), TRUE);
	}
}