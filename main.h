#pragma once

enum Images{Image_Logo,Stage_Lines};
enum ArrayImages{AImage_Pacman,AImage_Enemys,AImage_BreakPacman,AImage_Items,AImage_Scores};
enum Sounds { Sound_StartMusic, Sound_CoffeeBreak, Sound_ExtentSound, Sound_Eat_Cookies };

int LoadDate(void);
int LoadImages(void);
int LoadSounds(void);
int GetImages(int type);
int GetArrayImages(int type,const int num);
int GetSounds(int type);
int GetHighScore(void);
int LoadData(void);
int SaveData(void);