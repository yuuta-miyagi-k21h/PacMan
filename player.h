#pragma once
#include "Map.h"
#define	PLAYER_LIFE		3
#define PLAYER_INITX	13
#define PLAYER_INITY	23

enum PLAYER_STATUS{PLAYER_NORMAL};

class Player {
	int score;
	int x, y;
	int life;
	int speed;
	bool flirting_monster;		//モンスターを食べるモードにするフラグ
	bool deathflg;		//死亡時フラグ
	int Kefflg, PadTimer, PlayerControlTimer;
	int InputKey,JoyPadX, JoyPadY;
	int SelectX, SelectY;
	int AniTimer;		//アニメーション用タイマー
	int score_display;	//アイテム取得時のスコア表示用
	int sound_Power_cookie;	//パワークッキーを食べた際のモード音
	int ScoreFont;		//アイテム取得時のスコア表示用フォント
public:
	int Initialize(void);
	void DrawPlayer(Map &map,int type);
	void DeathPlayer(void);
	int UpdateAnalogInput(bool flg = true);
	int GetAnalogSelectInputX(void);
	int GetAnalogSelectInputY(void);
	int GetPlayerX(void);
	int GetPlayerY(void);
	int GetLife(void);
	void SetLife(int num);
	int GetScore(void);
	void AddScore(int num, bool display = false);
	bool Isflirting_monster(void);
	void Setflirting_monster(bool is);
};