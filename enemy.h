#pragma once
#include "player.h"
#include "Map.h"
#define Enemy_SpawnX	12
#define Enemy_SpawnY	14
#define GHOST_TIME		180

enum EnemyName{ SHADOW_BLINKY, SPEEDY_PINKY, POKEY_CLYDE, BASHFUL_INKY};

class Enemy {
	int type;
	bool flg;
	int x, y;
	int directionX, directionY;
	bool ghostflg;
	bool deathflg;
	int AniTimer;
	int sound_eat_ghost;		//ゴーストを食べた際SE
	int sound_ghost_normalmove;	//ゴースト平常移動時SE
	int sound_ghost_return;		//ゴーストが巣に戻るSE
public:
	int Initialize(void);
	void CreateEnemy(int type, Map& map, Player& player);
	void SHADOW_BLINKY_Move(Map map, Player player);
};