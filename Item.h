#pragma once
#include "Map.h"
#include "player.h"
#define Item_X		13
#define Item_Y		17

enum Item_type{Item_Cherry = 1, Item_Apple, Item_Strawberry, Item_Orange, Item_Melon, Item_Bell, Item_Key, Item_Boss};

class Item {
	int setitem;
	int timer;
	int sound_eat_fruit;
public:
	int Initialize(void);
	void SetItem(Map& map);
	int GetItem(Map& map, Player& player);
	int GetItemList(Map& map, Player& player);
	void DrawItem();
};