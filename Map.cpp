#include "Map.h"
#include "DxLib.h"
#include "main.h"

int Map::Initialize(void) {
	if (LoadDivGraph("Resource/Images/Stage/Lines.png", 8, 8, 1, 20, 20, Map_Lines) == -1)return -1;		//マップ上ライン
	
	for (int i = 0; i < MAP_HEIGHT; i++) {
		for (int j = 0; j < MAP_WIDTH; j++) {
			ResetMapDate[i][j] = MapDate[i][j];
		}
	}

	return 0;
}

void Map::DrawMap(void) {
	
	const int pointcolor = 0xF0CCCA;		//エサの色
	static int AniTimer, Flashingtime;
	static int arraycount;
	arraycount = 0;

	for (int i = 0; i < MAP_HEIGHT; i++) {
		for (int j = 0; j < MAP_WIDTH; j++) {
			//if ((MapDate[i][j] == 'B') || (MapDate[i][j] == 'X'))DrawBoxAA(static_cast<float>(MAP_SIZE * j + MAP_PositionX), static_cast<float>(MAP_SIZE * i + MAP_PositionY), static_cast<float>(MAP_SIZE * j + MAP_SIZE + MAP_PositionX), static_cast<float>((MAP_SIZE * i) + MAP_SIZE + MAP_PositionY), 0x000000, TRUE);	//空
			if (MapDate[i][j] == '|')DrawGraph(MAP_SIZE * j + MAP_PositionX, MAP_SIZE * i + MAP_PositionY, Map_Lines[0], TRUE);	//二重縦左線
			if (MapDate[i][j] == 'I')DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[0], TRUE, TRUE, FALSE);	//二重縦右線
			if (MapDate[i][j] == '-')DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, PI / 2, Map_Lines[0], TRUE, FALSE,TRUE);	//二重横上線
			if (MapDate[i][j] == 'U')DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, PI / 2, Map_Lines[0], TRUE, TRUE, TRUE);	//二重横下線
			if (MapDate[i][j] == 'q')DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, Map_Lines[1], TRUE);	//左上カーブ
			if (MapDate[i][j] == 'p')DrawTurnGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, Map_Lines[1], TRUE);		//右上カーブ
			if (MapDate[i][j] == 'z')DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[1], TRUE,FALSE,TRUE);		//左下カーブ
			if (MapDate[i][j] == 'm')DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[1], TRUE,TRUE,TRUE);	//右下カーブ
			if (MapDate[i][j] == 'R')DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, Map_Lines[6], TRUE);	//左上コーナー
			if (MapDate[i][j] == 'Q')DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0F, 0, Map_Lines[6], TRUE,TRUE);		//右上コーナー
			if (MapDate[i][j] == 'V')DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[6], TRUE,FALSE,TRUE);		//左下コーナー
			if (MapDate[i][j] == 'L')DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[6], TRUE,TRUE,TRUE);	//右下コーナー
			if (MapDate[i][j] == 'e')DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, Map_Lines[7], TRUE);	//巣左
			if (MapDate[i][j] == 'j')DrawTurnGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, Map_Lines[7], TRUE);	//巣右
			if (MapDate[i][j] == 'D') {
				DrawBoxAA(static_cast<float>(MAP_SIZE * j + MAP_PositionX), static_cast<float>(MAP_SIZE * i + MAP_PositionY), static_cast<float>(MAP_SIZE * j + MAP_SIZE + MAP_PositionX), static_cast<float>((MAP_SIZE * i) + MAP_SIZE + MAP_PositionY), 0x000000, TRUE);
				DrawLine(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2 + MAP_SIZE / 4, j * MAP_SIZE + MAP_PositionX + MAP_SIZE, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2 + MAP_SIZE / 4, 0xECBDFE, 3);	//巣出入り口
			}
			if (MapDate[i][j] == '.') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, Map_Lines[3], TRUE); }		//内線縦左
			if (MapDate[i][j] == ':') { DrawTurnGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, Map_Lines[3], TRUE); }		//内線縦右
			if (MapDate[i][j] == ',') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, PI / 2, Map_Lines[3], TRUE, FALSE, FALSE); }		//内線横左
			if (MapDate[i][j] == ';') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, PI / 2, Map_Lines[3], TRUE, TRUE, FALSE); }		//内線横右
			if (MapDate[i][j] == 'n') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, Map_Lines[5], TRUE); }		//内線上左カーブ
			if (MapDate[i][j] == 'u') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[5], TRUE, FALSE, TRUE); }		//内線下左カーブ
			if (MapDate[i][j] == 'r') { DrawTurnGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, Map_Lines[5], TRUE); }		//内線上右カーブ
			if (MapDate[i][j] == 'l') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[5], TRUE, TRUE, TRUE); }		//内線下右カーブ
			if (MapDate[i][j] == 'T') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[2], TRUE, FALSE, FALSE); }		//二重線ジョイント上左
			if (MapDate[i][j] == 'H') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[2], TRUE, TRUE, FALSE); }			//二重線ジョイント上右
			if (MapDate[i][j] == 'Y') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, PI / 2, Map_Lines[2], TRUE, FALSE, FALSE); }			//二重線ジョイント右上
			if (MapDate[i][j] == 'F') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, PI / 2, Map_Lines[2], TRUE, TRUE, FALSE); }			//二重線ジョイント右下
			if (MapDate[i][j] == 'W') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, PI / 2, Map_Lines[2], TRUE, FALSE, TRUE); }			//二重線ジョイント左上
			if (MapDate[i][j] == 'J') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, PI / 2, Map_Lines[2], TRUE, TRUE, TRUE); }			//二重線ジョイント左下

			if (MapDate[i][j] == 'v') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[4], TRUE, TRUE, FALSE); }		//ジョイント3左上
			if (MapDate[i][j] == 'b') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[4], TRUE, FALSE, FALSE); }		//ジョイント3右上
			if (MapDate[i][j] == 'i') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[4], TRUE, TRUE, TRUE); }		//ジョイント3左下
			if (MapDate[i][j] == 'x') { DrawRotaGraph(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2, i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2, 1.0f, 0, Map_Lines[4], TRUE, FALSE, TRUE); }		//ジョイント3右下

			if (MapDate[i][j] == '0') { 
				DrawBoxAA(static_cast<float>(MAP_SIZE * j + MAP_PositionX), static_cast<float>(MAP_SIZE * i + MAP_PositionY), static_cast<float>(MAP_SIZE * j + MAP_SIZE + MAP_PositionX), static_cast<float>((MAP_SIZE * i) + MAP_SIZE + MAP_PositionY), 0x000000, TRUE);
				DrawCircleAA(static_cast<float>(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2), static_cast<float>(i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2), 1.8f, 10, pointcolor, TRUE); }		//エサ
			if (MapDate[i][j] == 'o') {
				DrawBoxAA(static_cast<float>(MAP_SIZE * j + MAP_PositionX), static_cast<float>(MAP_SIZE * i + MAP_PositionY), static_cast<float>(MAP_SIZE * j + MAP_SIZE + MAP_PositionX), static_cast<float>((MAP_SIZE * i) + MAP_SIZE + MAP_PositionY), 0x000000, TRUE);
				if (++Flashingtime < 30)DrawCircleAA(static_cast<float>(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2), static_cast<float>(i * MAP_SIZE + MAP_PositionY + MAP_SIZE / 2), 10.0f, 10, pointcolor, TRUE);
				else if (Flashingtime > 60)Flashingtime = 0;
			}		//大エサ
			////			アイテム
			//if (MapDate[i][j] == 'c') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY + 10, GetArrayImages(AImage_Items,2), TRUE); }		//さくらんぼ
			//if (MapDate[i][j] == 'a') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Items,1), TRUE); }		//りんご
			//if (MapDate[i][j] == 's') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Items, 2), TRUE); }		//いちご
			//if (MapDate[i][j] == '@') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Items, 3), TRUE); }		//みかん
			//if (MapDate[i][j] == 'M') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Items, 4), TRUE); }		//メロン
			//if (MapDate[i][j] == 'S') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Items, 5), TRUE); }		//スイカ
			//if (MapDate[i][j] == 'k') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Items, 6), TRUE); }		//キー
			//if (MapDate[i][j] == 'g') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Items, 7), TRUE); }		//マスク

			//			キャラクター
			//if (MapDate[i][j] == 'P') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Pacman,2), FALSE); }		//パックマン

			{
				if (++AniTimer < 20) {
					if (MapDate[i][j] == 'A') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Enemys, 0), FALSE); }		//アカベイ
					if (MapDate[i][j] == 'K') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Enemys, 6), FALSE); }		//ピンキー
					if (MapDate[i][j] == 'O') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Enemys, 9), FALSE); }		//アオスケ
					if (MapDate[i][j] == 'Z') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Enemys, 15), FALSE); }		//グズタ
				}
				if (AniTimer >= 20) {
					if (MapDate[i][j] == 'A') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Enemys, 24), FALSE); }		//アカベイ
					if (MapDate[i][j] == 'K') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Enemys, 30), FALSE); }		//ピンキー
					if (MapDate[i][j] == 'O') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Enemys, 33), FALSE); }		//アオスケ
					if (MapDate[i][j] == 'Z') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Enemys, 39), FALSE); }		//グズタ
				}
				if (AniTimer >= 40) { AniTimer = 0; }
			}
			if (MapDate[i][j] == 'E') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Enemys, 16), FALSE); }		//目
			if (MapDate[i][j] == 'G') { DrawGraph(j * MAP_SIZE + MAP_PositionX, i * MAP_SIZE + MAP_PositionY, GetArrayImages(AImage_Enemys, 20), FALSE); }		//ゴースト
			//デバッグ用
			arraycount++;
			if (CheckHitKey(KEY_INPUT_SPACE)) { 
				DrawBoxAA(static_cast<float>(MAP_SIZE * j + MAP_PositionX), static_cast<float>(MAP_SIZE * i + MAP_PositionY), static_cast<float>(MAP_SIZE * j + MAP_SIZE + MAP_PositionX), static_cast<float>((MAP_SIZE * i) + MAP_SIZE + MAP_PositionY), 0xFFFFFF, FALSE, 1.0F);
				
				DrawExtendFormatString(j * MAP_SIZE + MAP_PositionX + MAP_SIZE / 2 - 15, i * MAP_SIZE + MAP_PositionY + MAP_SIZE, 0.32f, 0.32f, 0x00FFFF, "%3d", arraycount);
			}
		}
	}
}


char Map::GetMap(int x, int y) {
	if ((x >= 0 && MAP_WIDTH >= x) && (y >= 0 && MAP_HEIGHT >= y))return MapDate[y][x];
	return -1;
}

void Map::SetMap(int x, int y ,char type) {
	if ((x >= 0 && MAP_WIDTH >= x) && (y >= 0 && MAP_HEIGHT >= y))MapDate[y][x] = type;
}

void Map::ResetMap(void) {
	for (int i = 0; i < MAP_HEIGHT; i++) {
		for (int j = 0; j < MAP_WIDTH; j++) {
			MapDate[i][j] = ResetMapDate[i][j];
		}
	}
}