#include <iostream>
#include <fstream>
#include "DxLib.h"
#include "main.h"
#include "Map.h"
#include "player.h"
#include "enemy.h"
#include "Item.h"
#include "Title.h"
#include "HUD.h"

#define GAME_START_TIME		240

int GameState = 0;
int Score = 0;
int high_score;

int g_OldKey, g_NowKey, g_KeyFlg;
int JoyPadX, JoyPadY;
int start_time = GAME_START_TIME;		//開始時間

LPCSTR font_path = "Resource/Font/emulogic.ttf";
int img_Logo;
int img_ready;
int img_pacman[3];
int img_enemys[40];
int img_Items[8];
int img_scores[4];
int img_breakpacman[11];
int img_MapMask;

int sound_eat_cookies;		//クッキーを食べる音声

int Flashingtime;	//点滅時間
int sound_startmusic,sound_coffeebreak,sound_extentsound;	//BGM

int HiScore;	//ハイスコア格納用
double dNextTime = GetNowCount();

Map map;
Player player;
Enemy enemy;
Item item;
Title title;
HUD hud;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

	SetOutApplicationLogValidFlag(FALSE);   //ログ出力を無効にする
	SetWindowIconID(01);
	SetMainWindowText("パックマン");
	ChangeWindowMode(TRUE);
	SetWindowSize(1280, 720);
	SetGraphMode(1280, 720, 16);
	//SetBackgroundColor(256, 256, 256);		//背景を白に

		// フォント読み込み
	if (AddFontResourceEx(font_path, FR_PRIVATE, NULL) > 0) {
	}
	else {
		// フォント読込エラー処理
		MessageBox(NULL, "フォント読込失敗", "フォント読み込みエラー", MB_OK | MB_ICONERROR);
	}
	ChangeFont("Emulogic", DX_CHARSET_DEFAULT);
	SetFontSize(30);

	if (DxLib_Init() == -1)return -1;
	if (LoadImages() == -1)return -1;
	if (LoadSounds() == -1)return -1;
	if (LoadData() == -1)return -1;
	if (title.Initialize() == -1)return -1;
	if (map.Initialize() == -1)return -1;
	if (player.Initialize() == -1)return -1;
	if (enemy.Initialize() == -1)return -1;
	if (item.Initialize() == -1)return -1;
	item.SetItem(map);
	SetDrawScreen(DX_SCREEN_BACK);

	if (GetJoypadNum() == 0) {
		if (MessageBox(NULL, "コントローラーを接続してください。", "コントローラーが未接続です。", MB_OKCANCEL | MB_ICONWARNING) == 1)
			return -1;
	}

	while (ProcessMessage() == 0 && GameState != 99) {

		g_OldKey = g_NowKey;
		g_NowKey = GetJoypadInputState(DX_INPUT_PAD1);
		g_KeyFlg = g_NowKey & ~g_OldKey;
		if ( CheckHitKey(KEY_INPUT_ESCAPE) || g_KeyFlg & 1024) { GameState = 99; }
		


		ClearDrawScreen();

		if (title.DrawTitle(g_KeyFlg,map)) {
			//ライン背景用
			//DrawFillBox(MAP_PositionX, MAP_PositionY, MAP_PositionX + MAP_SIZE * MAP_WIDTH, MAP_PositionY + MAP_SIZE * MAP_HEIGHT, 0x3B50D7);
			hud.Draw_HUD(player);
			map.DrawMap();
			if (--start_time >= 0) {
				if(CheckSoundMem(sound_startmusic) == FALSE)PlaySoundMem(sound_startmusic, DX_PLAYTYPE_BACK, TRUE);
				player.UpdateAnalogInput(false);
				DrawRotaGraph(283 + MAP_PositionX, 100 + MAP_PositionY, 0.96f, 0, img_Logo, TRUE);
				DrawExtendString(178 + MAP_PositionX, 215 + MAP_PositionY, 0.65f,0.65f, "PLAYER ONE", 0x1FF3E8);
				DrawGraph(225 + MAP_PositionX, 340 + MAP_PositionY, img_ready, TRUE);
			}
			else {
				CreateMaskScreen();
				if (player.GetScore() > high_score) high_score = player.GetScore();
				DrawMask(MAP_PositionX - 2, MAP_PositionY, img_MapMask, DX_MASKTRANS_BLACK);
				//SetBackgroundColor(230, 200, 100);
				//SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
				//SetDrawBright(255 * 100 / 100, 255 * 100 / 100, 255 * 100 / 100);
				static int color = RGB(255, 255, 255);		//仮
				if (++Flashingtime < 9) {
					//DrawFillBox(MAP_PositionX, MAP_PositionY, MAP_PositionX + MAP_SIZE * MAP_WIDTH, MAP_PositionY + MAP_SIZE * MAP_HEIGHT, 0xFFFFFF); 
					DrawFillBox(MAP_PositionX - 2, MAP_PositionY, MAP_PositionX + MAP_SIZE * MAP_WIDTH, MAP_PositionY + MAP_SIZE * MAP_HEIGHT, color);
					color -= RGB(1, 1, 5);
				}
				else if (Flashingtime < 18);//DrawFillBox(MAP_PositionX, MAP_PositionY, MAP_PositionX + MAP_SIZE * MAP_WIDTH, MAP_PositionY + MAP_SIZE * MAP_HEIGHT, 0x3B50D7);
				else if (Flashingtime < 27) { Flashingtime = 0; }
				SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
				SetUseMaskScreenFlag(FALSE);
				player.UpdateAnalogInput();
				player.DrawPlayer(map, PLAYER_NORMAL);
				item.GetItem(map, player);
				enemy.CreateEnemy(SHADOW_BLINKY, map, player);


				//リスタート
				if (player.GetLife() == 0) {
					start_time = GAME_START_TIME;
					map.ResetMap();
					player.Initialize();
					enemy.Initialize();
					item.SetItem(map);
				}
			}
		}

		ScreenFlip();

		dNextTime += 16.66;
		if (dNextTime > GetNowCount()) {
			WaitTimer((int)dNextTime - GetNowCount());
		}



	}
	if (SaveData() == -1)return 0;
	InitFontToHandle();	//全てのフォントデータを削除
	InitMask();			//読み込んだ全てのマスクデータを削除
	InitGraph();		//読み込んだ全てのグラフィックデータを削除
	InitSoundMem();		//メモリに読み込んだ音データをすべて削除
	DxLib_End();

	return 0;
}

int LoadImages(void) {
	if ((img_Logo = LoadGraph("Resource/Images/packman_logo.png")) == -1)return -1;							//パックマンロゴ
	if ((img_ready = LoadGraph("Resource/Images/READY!.png")) == -1)return -1;							//パックマンロゴ
	if (LoadDivGraph("Resource/Images/Stage/packman.png", 3, 3, 1, 40, 40, img_pacman) == -1)return -1;		//パックマン
	if (LoadDivGraph("Resource/Images/Stage/BreakPacMan.png", 11, 11, 1, 15, 15, img_breakpacman) == -1)return -1;		//パックマン死亡
	if (LoadDivGraph("Resource/Images/Stage/Enemys.png", 40, 4, 10, 40, 40, img_enemys) == -1)return -1;		//エネミー
	if (LoadDivGraph("Resource/Images/Stage/Items.png", 8, 8, 1, 40, 40, img_Items) == -1)return -1;		//マップ上アイテム
	if (LoadDivGraph("Resource/Images/Stage/Enemy_Scores.png", 4, 4, 1, 40, 40, img_scores) == -1)return -1;		//スコア表示
	if ((img_MapMask = LoadMask("Resource/Images/Mask/Map_Mask.png")) == -1)return -1;							//マップマスク
	return 0;
}

int LoadSounds(void) {
	if ((sound_startmusic = LoadSoundMem("Resource/Sounds/BGM/StartMusic.wav")) == -1)return -1;
	if ((sound_coffeebreak= LoadSoundMem("Resource/Sounds/BGM/coffee break.wav")) == -1)return -1;
	if ((sound_extentsound = LoadSoundMem("Resource/Sounds/BGM/ExtentSound.wav")) == -1)return -1;
	if ((sound_eat_cookies = LoadSoundMem("Resource/Sounds/SE/Eat_cookies.wav")) == -1)return -1;
	return 0;
}

int GetImages(int type) {
	//if (type == Image_Logo) return Map_Logo;

	return 0;
}

int GetArrayImages(int type, int num) {
		switch (type)
		{
		case AImage_Pacman:
			if (0 <= num && num <= 2) {
				return img_pacman[num];
			}
			else { return -1; }
			break;

		case AImage_Enemys:
			if (0 <= num && num < 40) {
				return img_enemys[num];
			}else{return -1;}
			break;

		case AImage_BreakPacman:
			if (0 <= num && num < 11) {
				return img_breakpacman[num];
			}
			else { return -1; }
			break;

		case AImage_Scores:
			if (0 <= num && num < 4) {
				return img_scores[num];
			}
			else { return -1; }
			break;

		case AImage_Items:
			if (0 <= num && num < 8) {
				return img_Items[num];
			}
			else { return -1; }
			break;

		default:
			return -1;
			break;
		}
}

int GetSounds(int type) {
	if (Sound_StartMusic == type)return Sound_StartMusic;
	if (Sound_CoffeeBreak == type)return sound_coffeebreak;
	if (Sound_ExtentSound == type)return sound_extentsound;
	if (Sound_Eat_Cookies == type)return sound_eat_cookies;

	return 0;
}

int LoadDate(void) {
	return 0;
}

int LoadData(void) {

	std::ifstream ifs("Resource/Data.txt");

	if (!ifs) {
		std::ofstream ofs("Resource/Data.txt");
		if (!ofs) return -1;
	}

	ifs >> high_score;

	return 0;
}

int SaveData(void) {
	std::ofstream ofs("Resource/Data.txt");
	if (!ofs) return -1;

		ofs << high_score << std::endl;

	return 0;
}

int GetHighScore(void) {
	return high_score;
}